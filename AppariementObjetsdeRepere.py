"""
Created on Fri Oct 29 11:26:15 2021

@author: Jade Ternier et Jacqueline Williams.

Il existe de nombreuses bases de données géographiques qui contiennent forcément certains mêmes objets. Elles sont
modélisées différemment, les protocoles de saisies des données ne sont pas les mêmes, les taxonomies employées diffèrent
ce qui rend la tâche d’identification de ces mêmes objets difficile. Ce processus est appelé l’appariement de
données et consiste à mettre en correspondance les objets homologues représentant la même réalité.

Le but de ce projet consiste à apparier 2 jeux de données contenant des ponctuels représentant des objets de repère.
Le premier jeu provient de la BDTopo, base de données de référence produite par l’IGN. Le deuxième du site officiel de PNR Vercors

Les grandes étapes de ce projet sont donc :
} Charger les deux jeux de données 
} Charger la taxonomie des natures 
    Creation d'un dictionnaire 
} Implémenter les algorithmes des deux distances
    Distances Euclidienne 
    Distance Semantique
} Implémenter l’algorithme d’appariement en combinant les deux critères   
} Afficher les résultats de l’appariement sous forme de matrice de confusion.
"""

import csv
from collections import OrderedDict
import matplotlib.pyplot as plt

'variable globale : seuil distance , utile a letape de selection et letape decisives'
SEUIL_DISTANCE = 100


cheminversfichier= "C:/Users/jackgeo/Desktop/ENSG/S1/Projet_informatique/GitAppariementObjetsRepere/"

#############################################FONCTIONS########################################
#----------------------------------------------------------------------------------------------
#!!!# DEBUT FONCTIONS RELATIVES A L'ETAPE D'INITIATION
#----------------------------------------------------------------------------------------------
##FONCTIONS RELATIVES AUX CHARGEMENT DES DEUX JEUX DE DONNÉES

##fonction qui sert à récuperer uniquement les points de chaque ligne et de transtyper ces chaînes de caratères en float
##Renvoie une liste de deux elements le premier est le X et le second le Y du point.
def convertir_str_point(p):
    
    space = p.split()

    x = float(space[1].replace("(",""))
    y = float(space[2].replace(")",""))

    return [x, y]


##fonction qui sert à convertir une chaine de caratère en liste et de recupérer les points de chaque ligne.
## Fonction qui sert à creer une liste en prenant comme séparateur les ";", 
##elle transtype les chaines de caratère en float et retourne seulement une liste de deux elements le premier est le X et le second le Y du point.
##Utilisé pour le traitement BDTopo
def convertir_point_virgule(p):
    #print(p)
    chaine = p.split(";")
    #print(chaine,'Chaine')
    c = chaine[3].replace("POINT","")

    point = c.split()
    x = float(point[0].replace("(",""))
    y = float(point[1].replace(")",""))

    return [x, y]

##fonction pour récupérer que le type
def convertir_type(p):
    chaine = p.split(";")
    return chaine [2]

##affichage du fichier ftaxonomie sous forme de dico
def afficher_graph(g):
    print("{")
    for key in g.keys():
        print(len(g[key]), key , ":", g[key])
    print("}")

###FIN FONCTIONS RELATIVES AUX CHARGEMENT DES DEUX JEUX DE DONNÉES
#----------------------------------------------------------------------------------------------
#!!!# FIN FONCTIONS RELATIVES A L'ETAPE D'INITIATION
#----------------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------------
#!!!# DEBUT FONCTIONS RELATIVES A L'ETAPE DE CALCUL DES DISTANCES
#----------------------------------------------------------------------------------------------
#!!!##FONCTIONS RELATIVES AU CALCUL DE LA DISTANCE EUCLIDIENNE ET L'EMPRISE
##fonction qui calcul la distance entre deux points, renvoie la distance entre ces deux points
def distance_euclidienne(a,b):
    xa = a[0]
    xb = b[0]
    ya = a[1]
    yb = b[1]
    'equation de distance euclidienne'
    distance = ((xb -xa)**2+ (yb-ya)**2)**0.5

    return distance

'on aurait pu rassembler cet etape de calcul de distance eu lors de la fonction emprise en fait.'
##fonction qui à partir de la liste de points faite dans l'emprise retourne une liste des resultats des distances
def ndistanceRef_points(n , pp_points):
    liste_distances = []
    for p in pp_points:
        distance = distance_euclidienne(n , p)
        liste_distances.append(distance)

    return liste_distances

#!!!## FIN FONCTIONS RELATIVES AU CALCUL DE LA DISTANCE EUCLIDIENNE ET L'EMPRISE
#----------------------------------------------------------------------------------------------

#!!!##DEBUT FONCTIONS RELATIVES AU CALCUL DE LA DISTANCE SÉMANTIQUE
##fonction pour calculer la hauteur entre la nature et la racine du graph
def hauteur (dico,t_type):
    chemin = dico[t_type]
    return len(chemin)-1

##fonction pour la calculer la nature commune des types et renvoyer la nature
def nature_commune(dico, t_type1,t_type2):
    'on recupere le chemin des deux types'
    chemin1 = dico[t_type1]
    chemin2 = dico[t_type2]
    'on initialise la valeur de nature commune'
    nature = chemin[0]
    
    'longueur pour ne pas nous faire sortir de la boucle'
    longueur = min(len(chemin1),len(chemin2))
    for i in range (longueur):
        nature = chemin1[i]
        'break une fois que les types elles different'
        if chemin1[i] != chemin2[i]:
            break
    'nous renvoie la nature commune'
    return nature


##fonction pour calculer la hauteur de la nature commune aux deux types à la racine du graph
def hauteur_nature_commune(dico,t_type1,t_type2):
    nature = nature_commune(dico,t_type1,t_type2)
    hauteur_nature = hauteur(dico, nature)

    return hauteur_nature

##fonction pour calculer la distance sémantique retourne un résultat entre 0 et 1 plus proche de zéro = distance proche
def calcul_semantique(dico, t_type1,t_type2):
    
    h_type1 = hauteur(dico,t_type1 )
    h_type2 = hauteur(dico,t_type2 )
    Ncpc = hauteur_nature_commune(dico, t_type1,t_type2)

    if t_type1 == dico[t_type1][0] and t_type2 == t_type1:
        return 0

    'application de equation de Wu and Palmer'
    Dwp = 1 - ((2* Ncpc)/ (h_type1 + h_type2))
    return round(Dwp,2)


def n_calcul_pp_types_sem(dico,type_n,pp_types):
    'on initialise list vide qui recup distances sem'
    liste_semantique = []
    'pour tous les candidats'
    for i in range(len(pp_types)):
        'si leurs types est dans le dictionnaires (facultatif)'
        if pp_types[i] in dico.keys():
            'calcul de distance semantique pour ce type avec le point a apparier en question'
            semantique = calcul_semantique(dico, type_n, pp_types[i])
            'on nourri la liste'
            liste_semantique.append(semantique)
            'on renvoie la liste'
    return liste_semantique

###FIN FONCTIONS RELATIVES AU CALCUL DE LA DISTANCE SÉMANTIQUE
#----------------------------------------------------------------------------------------------
#!!!# FIN FONCTIONS RELATIVES A L'ETAPE DE CALCUL DES DISTANCES
#----------------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------------
#!!!# DEBUT FONCTIONS RELATIVES A L'ETAPE DE SELECTION
#----------------------------------------------------------------------------------------------
##fontion qui calcul la distance_euclidienne autour d'un points
##si condition vraie retourne deux listes une des types et une liste deux points    
def emprise(n, points_ref_type,dico):
    'on initialise deux liste, chacun correspond aux candidats pour un point'
    'pp_point recultera les coordonnes des candidats'
    pp_points = []
    'pp_types recolte les types des candidats'
    pp_types = []
    'pour tout les points dans la BD de  reference'
    for i in range(len(points_ref_type)):
        'calculer la distance euclidienne avec n'
        d = distance_euclidienne(n, points_ref_type[i][0])
        'si la distance est inferieur au seuil fourni en debut de code, ici 100m'
        if d <=SEUIL_DISTANCE and points_ref_type[i][1] in dico.keys():
            'on arrondi la distance pour lisibilité'
            d = round(d,2)
            
            'on garde les coordonnées des candidats dans cette liste'
            pp_points.append(points_ref_type[i][0])
            'on garde les types des candidats dans cette liste'
            pp_types.append(points_ref_type[i][1])


    return pp_types, pp_points
#----------------------------------------------------------------------------------------------
#!!!# FIN FONCTIONS RELATIVES A L'ETAPE DE SELECTION
#----------------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------------
#!!!# DEBUT FONCTIONS RELATIVES A L'ETAPE DECISIVES
#----------------------------------------------------------------------------------------------
### DEBUT FONCTIONS RELATIVES A L'ATTRIBUTION DE SCORE POUR APPARIER LES OBJETS

def seuils(pp_types,liste_semantique,liste_distance, seuil_semantique):
    pos='a'
    typeref=0
    seuil=0
    distance=-1
    gagnant=5
    for i in range(len(liste_semantique)):
        if liste_semantique[i]<= seuil_semantique and liste_distance[i]<=200:
            score=1
            if gagnant>score:
                gagnant=score
                pos=i
            
        elif liste_semantique[i]<=seuil_semantique and (liste_distance[i]>200 and liste_distance[i]<=SEUIL_DISTANCE):
            score=2
            if gagnant>score:
                gagnant=score
                pos=i
        elif liste_semantique[i]>seuil_semantique  and liste_distance[i]<=200:
            score=3
            if gagnant>score:
                gagnant=score
                pos=i
        elif liste_semantique[i]>seuil_semantique  and (liste_distance[i]>200 and liste_distance[i]<=SEUIL_DISTANCE):
            score=4
            if gagnant>score:
                gagnant=score
                pos=i
        else:
            return
        typeref=pp_types[pos]
        seuil=liste_semantique[pos]
        distance=round(liste_distance[pos], 2)
    return (typeref, seuil, distance)

### FIN FONCTIONS RELATIVES A L'ATTRIBUTION DE SCORE POUR APPARIER LES OBJETS
#----------------------------------------------------------------------------------------------
#!!!# FIN FONCTIONS RELATIVES A L'ETAPE DECISIVES
#----------------------------------------------------------------------------------------------


#----------------------------------------------------------------------------------------------
#!!!# DEBUT FONCTION APPARIEMENT OBJET DE REPERE
#----------------------------------------------------------------------------------------------
###FONCTIONS CENTRALE D'APPARIEMENT D'OBJET  
def appariement_obj(dico,points_ref_type,points_Vercors_type, seuil_semantique):
    'on initialise une liste vide pour recuperer nos resultats'
    liste_resultat = []
    'on dit --> pour chaque de notre BD à apparier'
    for i in range(len(points_Vercors_type)):
        'si le type est renseigné dans comme clé dans le dicitonnaire taxonoque'
        'alors on traite cet objet/ce point'
        if points_Vercors_type[i][1] in dico.keys():
            
            'on defini le type de n, c.a.d. le type de lobjet a apparier comme le type du point quon traite'
            typen = points_Vercors_type[i][1]
            
            '--etape calcul selection--'
            'on fait tourner la fonction emprise'
            'pp_types correspond aux types de tous les candidats possible de pour le point en question'
            pp_types = (emprise(points_Vercors_type[i][0], points_ref_type,taxo_chemins)[0])
            'pp_points correspond a toutes les coordonnés pour les candidats possible du point en question'
            pp_points = (emprise(points_Vercors_type[i][0], points_ref_type,taxo_chemins)[1])
            
            '--etape calcul distances--'
            'si il ny a pas de candidats possible on passe a lobjet suivant, il ny a pas de candidats possible'
            if len(pp_types) == 0:
                continue
            'sinon pour tout les candidats on calcul la distance eu '
            liste_distance = ndistanceRef_points(points_Vercors_type[i][0], pp_points)
            'sinon pour tout les candidats on calcul la distance sem '
            liste_semantique = n_calcul_pp_types_sem(taxo_chemins,typen,pp_types)
            
            '--etape decisive--'
            'on fourni la liste des dist sem et la liste des dist eu a la fonction seuils'
            typeref_seuil_distance = seuils(pp_types,liste_semantique, liste_distance, seuil_semantique)
            'si il y a eu appariement, il nous fourni le type du gagnant issue de la BD ref ainsi que les seuils utilisé'
            typeref, seuil, distance = typeref_seuil_distance
            
            'on a une pair dojet homologue quon implante dans la liste des resultat '
            liste_resultat.append((typen,typeref, seuil, distance))
        
            'on renvoie la liste des resultat'
    return liste_resultat
#----------------------------------------------------------------------------------------------
#!!!# FIN FONCTION APPARIEMENT OBJET DE REPERE
#----------------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------------
#!!!# DEBUT FONCTIONS AFFICHAGE
#----------------------------------------------------------------------------------------------
def creer_appariement_dico(liste_appariement):
    'initialisation dico'
    
    matrice = {}
    
    'Remplissage des resultats obtenu de la liste resultat'
    'Sert a compter les apparitions de chaque couple dobjets homologues'
    for types in liste_appariement:
        'cle composé'
        cle = types[0] +"_"+ types[1]
        'si cle nést pas deja dans dico on la cree'
        if cle not in matrice.keys():
            matrice[cle] = 0 
            
        'comptage doccurences'        
        val = matrice[cle]
        matrice[cle] = val + 1
    return matrice
    
def creer_noms_axes_matrice(liste_appariement):
    'on cree une lise avec toutes les types unique de chaque BDD pour pouvoir les mettre dans notres matrice'
    types_rando = []
    types_ref = []
    for appariement in liste_appariement:
        if appariement[0] not in types_rando:
            types_rando.append(appariement[0])
            
            
        if appariement[1] not in types_ref:
            types_ref.append(appariement[1])
            
    'nous rend les deux listes'   
    return types_rando , types_ref
        
        
        
        
def creer_matrice(liste_appariement, types_rando, types_ref):
    'squelette matric compose de 0 pour les valeurs absente et sinon de la valeur associe à la cle composé'
    dico_appariement = creer_appariement_dico(liste_appariement)
   
    matrice = []
    
    for t_ref in types_ref:
        
        ligne = []
        for t_rando in types_rando:
            cle = t_rando + "_" + t_ref
            if cle in dico_appariement.keys():
                
                ligne.append(dico_appariement[cle])
            else:
                ligne.append(0)
                
        matrice.append(ligne)
            
    return matrice 
#----------------------------------------------------------------------------------------------
#!!!# FIN FONCTIONS AFFICHAGE
#----------------------------------------------------------------------------------------------

#_______________________________________________________________________________________________    
##########################################DEBUT DU PROGRAMME##################################
#-------------------------------------------------------------------------------------------
#!!!# DEBUT CHARGEMENT DONNEES
#-------------------------------------------------------------------------------------------
###CHARGEMENT JEUX DE DONNES 1 - BD TOPO, BDD DE RÉFÉRENCE
'ouverture du fichier csv'
f = open(cheminversfichier+'pai_bdtopo_vercors.csv',encoding='utf-8')
'lecture du fichier csv'
fichierBDtopo = csv.reader(f)
###Nous initialisons une liste correspondant aux colonnes coordonnées et type
'compte ligne qui nous sert a ignorer la premiere ligne de titre de colonnes dans un fichier csv'
nb_ligne = 0
'on initilialise la fiche qui resumera toute les informations relatives a apparier de notre BDD de reference'
BDRef=[]
'pour toutes les lignes du csv'
for ligne in fichierBDtopo:
    'on ignore la premiere ligne'
    nb_ligne += 1
    if nb_ligne == 1:
        continue
    'on dit que pour ce fichier le type correspond a tel colonne, et que par consequent le type de lobjet et la valeur sur cette colonne de telle ligne'
    Type=ligne[2]
    'on enleve toutes les majuscules et caracteres speciaux, il y avait des manieres plus efficaces de faire cela'
    Type=str.lower(Type)
    Type=Type.replace(' ','')
    Type=Type.replace('_','')
    Type=Type.replace('é','e')
    Type=Type.replace('è','e')
    Type=Type.replace('â','a')
    Type=Type.replace('ê','e')
    Type=Type.replace('\'','')
    

    # print(points_ref_type)
    
    'on dit que colonne [0] correspond au coordonnes point et que le point correspond a cette colonne de telle ligne'
    point_str = ligne [0]
    'on converti le point en float'
    point=convertir_str_point(point_str)

    'on nourri notre liste recapitulatives de la BDD de reference'
    BDRef.append((point,Type))


#!!!###CHARGEMENT JEUX DE DONNES 1 - BD VERCORS, BDD DE RÉFÉRENCE
'meme idee que le premier fichier csv'
f1 = open(cheminversfichier+'pai_parc_vercors.csv', encoding='utf-8')
fichierAppariement = csv.reader(f1,delimiter = ";")

BDobjets=[]
nb_ligne = 0
# #for qui recupère des points du fichier Vercors
for ligne in fichierAppariement:
    nb_ligne +=1
    if nb_ligne == 1:
        continue
 
    'le traitement des coordonnées de point a varié un petit peu'    
    point_str = ligne[3]
    point=[]
    Point=point_str.replace('POINT(', '')
    Point=Point.replace(')','')
    Point=Point.replace(',','')
    Point=Point.split(' ')
    x=float(Point[0])
    y=float(Point[1])
    point=[x,y]
    
    Type=ligne[2]
    Type=str.lower(Type)
    Type=Type.replace(' ','')
    Type=Type.replace('_','')
    Type=Type.replace('é','e')
    Type=Type.replace('è','e')
    Type=Type.replace('â','a')
    Type=Type.replace('ê','e')
    Type=Type.replace('\'','')
    
    'on change eau à eaux pour augmenter nos resultats dappariements'
    if Type == 'eau':

        Type = 'eaux'
    
    'on nourri notre liste recapitulatives de la BDD dobjets a apparier'
    BDobjets.append((point,Type))
#-------------------------------------------------------------------------------------------
#!!!# FIN CHARGEMENT FICHIER TAXONOMIE 
#-------------------------------------------------------------------------------------------

#-------------------------------------------------------------------------------------------
#!!!# DEBUT CHARGEMENT FICHIER TAXONOMIE 
#-------------------------------------------------------------------------------------------
'on ouvre le fichier txt de taxonomie'
fichiertxt = open (cheminversfichier+'OOR.txt',encoding='utf-8')
'fTaxonomie est une liste qui va recupere tous les cheminement du fichier taxo sous forme de sous-liste'
fTaxonomie = []
for chemin in fichiertxt:
    ##Formattage de nos chemin de type pour obtenir un format obtenir une liste position Integral[0]
    'on formatte la ligne pour faire une liste'
    chemin=str(chemin)
    chemin= chemin.replace('\n', '')
    chemin= chemin.split("#")
    'pour chaque element de la liste on enleve les caracteres speciaux'
    for i in range(len(chemin)):
        chemin[i]=str.lower(chemin[i])
        chemin[i]=chemin[i].replace(' ','')
        chemin[i]=chemin[i].replace('_','')
        chemin[i]=chemin[i].replace('é','e')
        chemin[i]=chemin[i].replace('è','e')
        chemin[i]=chemin[i].replace('â','a')
        chemin[i]=chemin[i].replace('ê','e')
        chemin[i]=chemin[i].replace('\'','')
    
    'on nourri le fichier integral de taxo avec sa sous liste'
    fTaxonomie.append(chemin)
    
ftaxo = sorted(fTaxonomie, key = len)#trier par longueur de chemin

taxo_chemins = {'Thing':['Thing']}

for chemin in ftaxo:
    #print(ftaxo)
    if chemin[-1] == "":
        continue
    else:

        if chemin[-1] in taxo_chemins.keys():

            continue

        else:

            taxo_chemins[chemin[-1]]= chemin

taxo_chemins = OrderedDict(sorted(taxo_chemins.items(), key = lambda t:len(t[1])))
#afficher_graph(taxo_chemins)
#-------------------------------------------------------------------------------------------
#!!!# FIN CHARGEMENT FICHIER TAXONOMIE 
#-------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------
#!!!# DEBUT ETUDES DE DONNÉES
#----------------------------------------------------------------------------------------------
### IMPRESSION DE TOUS LES TYPES UNIQUES DE NOS DEUX BDD
### Pour etudier le document nous avons etudier tous les elements unique de type pour voir si il y avait correspondance entre nos fichiers excel
### Nous pourrons aussi voir si il y a correspondance avec ces types dans notre fichier taxonomie
DistinctBDobjets=[]
for i in range(len(BDobjets)):
    if BDobjets[i][1] not in DistinctBDobjets:
        DistinctBDobjets.append(BDobjets[i][1])
# print(DistinctBDobjets)

DistinctBDRef=[]
for i in range(len(BDRef)):
    if BDRef[i][1] not in DistinctBDRef:
        DistinctBDRef.append(BDRef[i][1])
#print(DistinctBDRef)
# # ##FIN ETUDE DES VALEURS UNIQUE BDD

###IMPRESSION DE TOUS LES TYPES UNIQUES DE NOTRE FICHIER TAXONOMIE
DistinctTaxonomie = []
for i in range(len(fTaxonomie)-1):
    for j in range(len(fTaxonomie[i])):
        if fTaxonomie[i][j] not in DistinctTaxonomie:
            DistinctTaxonomie.append(str.lower(fTaxonomie[i][j]))           
# print(DistinctTaxonomie)
# ##FIN ETUDE DES VALEURS UNIQUE TAXONOMIE

##CALCUL POURCENTAGE DE TYPE DES BDD PRESENT DANS LE FICHIER TAXONOMIE
'on compte les types qui sont present dans le fichier taxonomique'
compteObjTypes = sum(el in DistinctBDobjets for el in DistinctTaxonomie)
compteRefTypes = sum(el in DistinctBDRef for el in DistinctTaxonomie)

'on calcul notre poucentage de presence'
ratioObjTypes = (compteObjTypes/len(DistinctBDobjets))*100
ratioRefTypes = (compteRefTypes/len(DistinctBDRef))*100

print(str(ratioRefTypes)+'% des types des objets de notre table de reference sont present dans notre fichier taxonomie')
print(str(ratioObjTypes)+'% des types des objets de notre table a apparié sont present dans notre fichier taxonomie')
##FIN CALCUL POURCENTAGE
#----------------------------------------------------------------------------------------------
#!!!# FIN FONCTIONS ETUDES DE DONNÉES
#----------------------------------------------------------------------------------------------

#-------------------------------------------------------------------------------------------
#!!!# DEBUT APPEL A LA FONCTION APPARIEMENT OBJET REPERE
#-------------------------------------------------------------------------------------------
liste_resultat = appariement_obj(taxo_chemins,BDRef,BDobjets, 0.5)
#print(liste_resultat)
#print(*liste_resultat, sep = '\n')
#print(len(liste_resultat), "correspondances")
#-------------------------------------------------------------------------------------------
#!!!# FIN APPEL A LA FONCTION APPARIEMENT OBJET REPERE
#-------------------------------------------------------------------------------------------

#-------------------------------------------------------------------------------------------
#!!!# DEBUT APPEL AUX FONCTIONS AFFICHAGE
#-------------------------------------------------------------------------------------------          
types =  creer_noms_axes_matrice(liste_resultat)
types_rando = types[0]
types_ref = types[1]   
dico_appariement = creer_appariement_dico(liste_resultat)
#print(creer_appariement_dico(liste_resultat))
# 
# # a 2D array with linearly increasing values on the diagonal
matrice = creer_matrice(liste_resultat,types_rando,types_ref)

fig, ax = plt.subplots()
# #Noms des axes: 

ax.matshow(matrice, cmap=plt.cm.YlGn, vmin=0, vmax=10)

plt.xticks(range(len(types_rando)), types_rando, rotation=60)
plt.yticks(range(len(types_ref)),types_ref)

# pour remplir les valeurs dans la matrice 
for i in range(len(types_rando)):
    for j in range(len(types_ref)):
        cle = types_rando[i]+"_"+types_ref[j]
        if cle in dico_appariement.keys():
            
            c = dico_appariement[cle]
            ax.text(i, j, str(c), va='center', ha='center')
        else: 
            ax.text(i, j, str(0), va='center', ha='center')
        
plt.suptitle("Matrice de confusion appariement")
plt.title( "Appariements : " + str(len(liste_resultat)) + ' Emprise : ' + str(SEUIL_DISTANCE) + 'm ')
ax.set_xlabel('Types objets à apparier ')
ax.set_ylabel('Types objets de référence')

plt.tight_layout()
plt.show()

#-------------------------------------------------------------------------------------------
#!!!# FIN APPEL AUX FONCTIONS AFFICHAGE
#-------------------------------------------------------------------------------------------  


















